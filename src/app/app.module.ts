import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HttpModule } from '@angular/http';

import { TabsPage } from '../pages/tabs/tabs';
import { AnimalPage } from '../pages/animal/animal';
import { AtendimentoPage } from '../pages/atendimento/atendimento';
import { FormAnimalPage } from '../pages/form-animal/form-animal';
import { FormAtendimentoPage } from '../pages/form-atendimento/form-atendimento';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AnimalProvider } from '../providers/animal/animal';
import { AtendimentoProvider } from '../providers/atendimento/atendimento';

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    AnimalPage,
    AtendimentoPage,
    FormAnimalPage,
    FormAtendimentoPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    AnimalPage,
    AtendimentoPage,
    FormAnimalPage,
    FormAtendimentoPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AnimalProvider,
    AtendimentoProvider
  ]
})
export class AppModule {}
