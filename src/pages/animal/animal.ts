import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { FormAnimalPage } from '../form-animal/form-animal';
import { AnimalProvider } from '../../providers/animal/animal';

@IonicPage()
@Component({
  selector: 'page-animal',
  templateUrl: 'animal.html',
})
export class AnimalPage {

  animais : Array <any>;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private alertCtrl : AlertController,
    private animalService : AnimalProvider
    ) {
    this.selectAll();
  }

  selectAll () {
    this.animalService.findAll ().then ((resposta) => {
      let json = resposta.json ();
      this.animais = json.result.animais;
    }).catch ((erro) => {
      console.log ("Erro ao recuperar os Animais: " + erro);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AnimalPage');
  }

  abreFormAnimal(){
  	let modal = this.modalCtrl.create(FormAnimalPage);
    modal.onDidDismiss (() => this.selectAll());
    modal.present();
  }

  alteraAnimal(animal : any) {
    let modal = this.modalCtrl.create (FormAnimalPage, {animal : animal});
    modal.onDidDismiss (() => this.selectAll());
    modal.present ();
  }

  removeAnimal (animal : any) {
    let alerta = this.alertCtrl.create ({
      title : 'Remover Animal',
      message : 'Remove o Animal: ' + animal.nome,
      buttons : [
      {text : 'Cancelar'},
      {
        text : 'Remover',
        handler : (dados) => {
          this.animalService.remove (animal.id).then ((resposta) => this.selectAll ()
            ).catch ((erro) => {
              let alert1 = this.alertCtrl.create ({
                title : 'Erro!',
                message : 'Erro na remoção do animal: ' + erro,
                buttons : [
                {text : 'Ok'}
                ]
              });
              alert1.present();
            });
          }
        }
        ]
      });
    alerta.present ();
  }
}