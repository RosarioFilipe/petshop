import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { FormAtendimentoPage } from '../form-atendimento/form-atendimento';
import { AtendimentoProvider } from '../../providers/atendimento/atendimento';

@IonicPage()
@Component({
  selector: 'page-atendimento',
  templateUrl: 'atendimento.html',
})
export class AtendimentoPage {

  atendimentos : Array <any>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private alertCtrl : AlertController,
    private atendimentoService : AtendimentoProvider
    ) {
    this.selectAll();
  }

  selectAll () {
    this.atendimentoService.findAll ().then ((resposta) => {
      let json = resposta.json ();
      this.atendimentos = json.result.atendimentos;
    }).catch ((erro) => {
      console.log ("Erro ao recuperar os Atendimentos: " + erro);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AtendimentoPage');
  }

  abreFormAtendimento(){
    let modal = this.modalCtrl.create(FormAtendimentoPage);
    modal.onDidDismiss (() => this.selectAll());
    modal.present();
  }

  alteraAtendimento(atendimento : any) {
    let modal = this.modalCtrl.create (FormAtendimentoPage, {atendimento : atendimento});
    modal.onDidDismiss (() => this.selectAll());
    modal.present ();
  }

  removeAtendimento (atendimento : any) {
    let alerta = this.alertCtrl.create ({
      title : 'Remover Atendimento',
      message : 'Remove o Atendimento: ' + atendimento.nome,
      buttons : [
      {text : 'Cancelar'},
      {
        text : 'Remover',
        handler : (dados) => {
          this.atendimentoService.remove (atendimento.id).then ((resposta) => this.selectAll ()
            ).catch ((erro) => {
              let alert1 = this.alertCtrl.create ({
                title : 'Erro!',
                message : 'Erro na remoção do atendimento: ' + erro,
                buttons : [
                {text : 'Ok'}
                ]
              });
              alert1.present();
            });
          }
        }
        ]
      });
    alerta.present ();
  }
}