import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormAnimalPage } from './form-animal';

@NgModule({
  declarations: [
    FormAnimalPage,
  ],
  imports: [
    IonicPageModule.forChild(FormAnimalPage),
  ],
})
export class FormAnimalPageModule {}
