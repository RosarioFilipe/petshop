import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AnimalProvider } from '../../providers/animal/animal';

@IonicPage()
@Component({
  selector: 'page-form-animal',
  templateUrl: 'form-animal.html',
})
export class FormAnimalPage {

  animal : any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public animalService : AnimalProvider
    ) {
    this.animal = navParams.get('animal') || {};
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormAnimalPage');
  }

  voltarPagina(){
    this.navCtrl.pop();
  }

  salvar() {
    if (this.animal.id == undefined) {
      this.animalService.insere(this.animal).then((res) => {
        this.navCtrl.pop();
      });
    } else {
      this.animalService.atualiza(this.animal).then((res) => {
        this.navCtrl.pop();
      });
    }
  }
}