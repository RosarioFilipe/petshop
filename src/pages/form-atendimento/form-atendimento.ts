import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AtendimentoProvider } from '../../providers/atendimento/atendimento';
import { AnimalProvider } from '../../providers/animal/animal';

@IonicPage()
@Component({
  selector: 'page-form-atendimento',
  templateUrl: 'form-atendimento.html',
})
export class FormAtendimentoPage {

  animais : Array <any>;
  atendimento : any;

  constructor(
  	public navCtrl: NavController,
  	public navParams: NavParams,
    public atendimentoService : AtendimentoProvider,
    public animalService : AnimalProvider
    ) {
    this.atendimento = navParams.get('atendimento') || {};

    this.animalService.findAll().then(
      (r) => {
        let json = r.json(); 
        this.animais = json.result.animais;
      }
    ).catch (
      (e) => console.log ("Erro na obtenção dos Animais: " + e)
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormAtendimentoPage');
  }

  voltarPagina(){
    this.navCtrl.pop();
  }

  salvar() {
    if (this.atendimento.id == undefined) {
      this.atendimentoService.insere(this.atendimento).then ((res) => {
        this.navCtrl.pop();
      });
    } else {
      this.atendimentoService.atualiza(this.atendimento).then ((res) => {
        this.navCtrl.pop();
      });
    }
  }
}