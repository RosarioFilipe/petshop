import { Component } from '@angular/core';

import { AnimalPage } from '../animal/animal';
import { AtendimentoPage } from '../atendimento/atendimento';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = AnimalPage;
  tab2Root = AtendimentoPage;
  
  constructor() {

  }
}
