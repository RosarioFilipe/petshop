import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AnimalProvider {

	baseURL : string;

  constructor(
      public http: Http  
    ) {
    this.baseURL = "http://petshop-api.herokuapp.com/animais";
  }

  findAll () : Promise <Response> {
    return this.http.get(this.baseURL).toPromise();
  }

  remove (id) : Promise <Response> {
    return this.http.delete(this.baseURL + '/' + id).toPromise();
  }

  atualiza (animal) : Promise <Response> {
    let cabecalho = new Headers ();
    cabecalho.append ('Content-type', 'application/json');

    return this.http.put(this.baseURL + '/' + animal.id, JSON.stringify(animal), {headers : cabecalho}).toPromise();
  }

  insere (animal) : Promise <Response> {
    let cabecalho = new Headers ();
    cabecalho.append ('Content-type', 'application/json');

    return this.http.post(this.baseURL, JSON.stringify(animal), {headers : cabecalho}).toPromise();
  }
}