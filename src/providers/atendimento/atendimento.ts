import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AtendimentoProvider {

	baseURL : string;

  constructor(
  	  public http: Http
  	) {
  	this.baseURL = "http://petshop-api.herokuapp.com/atendimentos";
  }

  findAll () : Promise <Response> {
    return this.http.get(this.baseURL).toPromise();
  }

  remove (id) : Promise <Response> {
    return this.http.delete(this.baseURL + '/' + id).toPromise();
  }

  atualiza (atendimento) : Promise <Response> {
    let cabecalho = new Headers ();
    cabecalho.append ('Content-type', 'application/json');

    return this.http.put(this.baseURL + '/' + atendimento.id, JSON.stringify(atendimento), {headers : cabecalho}).toPromise();
  }

  insere (atendimento) : Promise <Response> {
    let cabecalho = new Headers ();
    cabecalho.append ('Content-type', 'application/json');

    return this.http.post(this.baseURL, JSON.stringify(atendimento), {headers : cabecalho}).toPromise();
  }
}